package com.michalrys.spring10_mvc_data_posgresql.persistance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ClientAddress {
    @Id
    @GeneratedValue
    private Long id;
    private String address;

    public ClientAddress() {
    }

    public ClientAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
