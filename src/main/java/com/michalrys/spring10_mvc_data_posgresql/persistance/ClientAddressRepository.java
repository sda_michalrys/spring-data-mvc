package com.michalrys.spring10_mvc_data_posgresql.persistance;

import org.springframework.data.repository.CrudRepository;

public interface ClientAddressRepository extends CrudRepository<ClientAddress, Long> {
}
