package com.michalrys.spring10_mvc_data_posgresql.tools;

import com.michalrys.spring10_mvc_data_posgresql.persistance.Client;
import com.michalrys.spring10_mvc_data_posgresql.persistance.ClientAddress;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ClientTestDataService {

    public List<Client> getClients() {
        Client client1 = new Client("Tom", "Hanks");
        ClientAddress clientAddress1 = new ClientAddress("Krakow, Balicka 12");
        client1.setClientAddress(clientAddress1);

        Client client2 = new Client("Micky", "Mouse");
        ClientAddress clientAddress2= new ClientAddress("Poznan, Balicka 13");
        client2.setClientAddress(clientAddress2);

        Client client3 = new Client("Monika", "Lewinsky");
        ClientAddress clientAddress3= new ClientAddress("Opole, Balicka 14");
        client3.setClientAddress(clientAddress3);

        Client client4 = new Client("Bill", "Clinton");
        ClientAddress clientAddress4= new ClientAddress("Tarnow, Balicka 15");
        client4.setClientAddress(clientAddress4);

        return Arrays.asList(client1, client2, client3, client4);
    }
}
