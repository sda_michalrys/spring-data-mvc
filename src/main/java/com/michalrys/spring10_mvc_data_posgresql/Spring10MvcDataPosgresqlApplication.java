package com.michalrys.spring10_mvc_data_posgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring10MvcDataPosgresqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spring10MvcDataPosgresqlApplication.class, args);
    }

}
