package com.michalrys.spring10_mvc_data_posgresql.web;

import com.michalrys.spring10_mvc_data_posgresql.persistance.Client;
import com.michalrys.spring10_mvc_data_posgresql.persistance.ClientRepository;
import com.michalrys.spring10_mvc_data_posgresql.tools.ClientTestDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientTestDataService clientTestDataService;

    @GetMapping("/read")
    public List<Client> readData() {
        List<Client> result = new ArrayList<>();
        clientRepository.findAll().forEach(client -> result.add(client));
        return result;
    }

    @GetMapping("/save")
    public void saveData() {
        List<Client> clients = clientTestDataService.getClients();
        clientRepository.saveAll(clients);
    }
}
